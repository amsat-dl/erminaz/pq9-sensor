# PQ9 Sensor Board

~image~

This is a sensor board for radiation including a 9-axis IMU

## Dependencies

This design uses the Libre Space Foundation KiCAD Library [lsf-kicad-lib](https://gitlab.com/librespacefoundation/lsf-kicad-lib#usage). Follow [this guide](https://gitlab.com/librespacefoundation/lsf-kicad-lib#usage) for local usage.


## License
&copy; 2023 AMSAT-DL & committers

Licensed under the [CERN OHLv1.2](LICENSE).
